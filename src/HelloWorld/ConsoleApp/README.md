## How to run

* Build the docker image with:

```
docker build --tag dotnet-core-sample-image .
```

* Run the Docker image with:

```
docker run dotnet-core-sample-image
```
