# specify the base image
FROM microsoft/dotnet:2.2-sdk AS build-env

# set the working directory to 'app'
WORKDIR /app

# copy project file
COPY *.csproj ./

# restore as distinct layers
RUN dotnet restore

# copy restored dependencies
COPY . ./

# build and publish the project with all dependecies needed
RUN dotnet publish --configuration="Release" --output="out"

# build runtime image
FROM microsoft/dotnet:2.2-aspnetcore-runtime

# set the working directory to 'app'
WORKDIR /app

# copy the base image to application output directory
COPY --from=build-env /app/out .

# instruct the docker to run as a executable (dotnet.exe process)
ENTRYPOINT ["dotnet", "WebApp.dll"]
