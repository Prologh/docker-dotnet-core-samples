# Build Docker images with .NET Core

README 03 March 2019

A set of few, minimalistic samples showing how to build Docker images for .NET Core applications.

## Prerequirements

* .NET Core SDK 2.2 or later ([download here](https://dotnet.microsoft.com/download)) (optional)
* Docker ([download here](https://www.docker.com/get-started))

## How to run

#### 1) Build and run application locally (optional)

* Clone the repository
* Go to the sample you want e.g. "HelloWorld"
* Open you favourite command line interpreter (CLI)
* Build the project with:

```
dotnet build
```

* Run the project with:

```
dotnet run
```

If the output does not throw any errors, continue to the next step.


#### 2) Build and run Docker image

* Clone the repository
* Go to the sample project you want for example console hello world app
* Open you favourite command line interpreter (CLI)
* Build the docker image with:

```
docker build --tag dotnet-core-sample .
```

* Run the Docker image with:

```
docker run dotnet-core-sample --rm
```

## License

Licensed under MIT. See full license [here](https://gitlab.com/Prologh/docker-dotnet-core-samples/blob/master/LICENSE).